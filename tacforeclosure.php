<?php
include 'class/Config.php';
include 'class/CsvParser.php';
include 'class/DbConnect.php';

class Parser extends CSVPARSER
{
	private $data;
	private $dir = "C:/ScrapeData/tacforeclosure/";
	private $objqry;
	private $qryString;
	private $files;
	private $stateDir;
		
	public function __construct()
	{
		$this->objqry = new Database();
	}
	
	
	public function parsetacforeclosure()
	{
		$this->stateDir = scandir($this->dir);
		
		$this->emptyTable();
		
		$this->parseData($this->dir);
		
	}
	
	private function parseData($state)
	{
		echo $this->dir.$state;
		$this->files = glob($this->dir."/*.csv");
		echo "<pre>+".$state."</pre>";
		foreach($this->files as $file):
			if(isset($file) && !empty($file)):
				echo $file;
				$this->parse($file);
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+".$file."<br>";
			endif;
		endforeach;
	}
	
	private function parse($csvfile)
	{
		$this->data = parent::convertToArray($csvfile); 
		$i = 0;
		foreach($this->data as $data):
			if($i>0):
				$this->qryString = "`property_address` = '".mysql_real_escape_string($data[10])."', 
									`trustee_sale_number` = '".mysql_real_escape_string($data[1])."', 
									`county` = '".mysql_real_escape_string($data[7])."', 
							   		`opening_bid` = '".mysql_real_escape_string($data[8])."', 
									`sale_date` = '".mysql_real_escape_string(self::saleDate($data[9]))."' 
									";
				$this->objqry->queryInsert("INSERT INTO `update_tacforeclosure` SET ".$this->qryString);
				
				
			endif;
			$i++;
		endforeach;
	}
	

	
	private function emptyTable()
	{
		$this->objqry->queryExecute("TRUNCATE `update_tacforeclosure`");
	}
}

$parser = new Parser();
$parser->parsetacforeclosure();

