<?php
include 'class/Config.php';
include 'class/Lpsasap.php';
include 'class/DbConnect.php';

class LpsParser extends Lpsasap
{
	private $data;
	private $dir = "C:/ScrapeData/lps/";
	private $objqry;
	private $qryString;
	private $files;
	private $stateDir;
		
	public function __construct()
	{
		$this->objqry = new Database();
	}
	
	public function parseLps()
	{
		$this->stateDir = scandir($this->dir);
		
		$this->emptyTable();
		
		foreach($this->stateDir as $state):
			if(isset($state) && !empty($state)):
				$this->parseData($state);
			endif;
		endforeach;
	}
	
	private function parseData($state)
	{
		$this->files = glob($this->dir.$state."/*.csv");
		if(sizeof($this->files)):
			echo "<pre>+".$state."</pre>";
			foreach($this->files as $file):
				if(isset($file) && !empty($file)):
					$this->parse($file);
				endif;
			endforeach;
		endif;
	}
	
	private function parse($csvfile)
	{
		$this->data = parent::convertToArray($csvfile); 
		if(sizeof($this->data) > 1):
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+".$csvfile."<br>";
			$i = 0;
			foreach($this->data as $data):
				if($i>0):
					$this->qryString = "`street` = '".mysql_real_escape_string($data[2])."', 
										`city` = '".mysql_real_escape_string($data[3])."', 
										`state` = '".mysql_real_escape_string($data[4])."', 
										`zipcode` = '".mysql_real_escape_string(self::zipCode($data[5]))."', 
										`county` = '".mysql_real_escape_string($data[6])."',
										`trustee_sale_number` = '". mysql_real_escape_string($data[1])."',
										`sale_date` = '". mysql_real_escape_string(self::saleDate($data[0]))."',
										`status` = '".mysql_real_escape_string($data[8])."',
										`nos_amount` = '".mysql_real_escape_string($data[9])."',
										`opening_bid` = '".mysql_real_escape_string($data[10])."',
										`sold_amount` = '".mysql_real_escape_string($data[11])."',
										`sale_place` = '".mysql_real_escape_string(self::salePlace($data[12],'AZ'))."',
										`trustee_name` = '".mysql_real_escape_string($data[13])."' ";
										
					$this->objqry->queryInsert("INSERT INTO `update_lpsasap` SET ".$this->qryString);
				endif;
				$i++;
			endforeach;
		endif;	
	}
	
	private function emptyTable()
	{
		$this->objqry->queryExecute("TRUNCATE `update_lpsasap`");
	}
}

$lparser = new LpsParser();
$lparser->parseLps();

