<?php
class CSVPARSER {
	#var $_csvLocation = "";

	function __construct(){
		
	}
	
	function convertToArray($csvLocation){
		$data = array();
		$file = fopen($csvLocation, 'r');
		while (($line = fgetcsv($file)) !== FALSE) {
			$data[] = $line;

		}
		fclose($file);
		
		return $data;
	}
	
	function cleanTrusteeName($string){
		$patterns = array("/RECONTRUST/");
		
		foreach ($patterns as $pattern){
			$match = array();
			preg_match($pattern, $string, $match);
			
			if(!empty($match)){
				break;
			}
		}
		
		return trim($match[0]);
	}
	
	function propertyAddress($string){
		$patterns = array("/([a-zA-Z0-9\,\.\#\&\;\s\-\)\(]+)([0-9\-])/",
										   "/([0-9]{2,5})\s?([a-zA-Z0-9\,\.\#\&\;\s\-\)\(]+)([0-9]{5})/",
										   "/([0-9]{2,5})\s?([a-zA-Z0-9\,\.\#\&\;\s\-\)\(]+)/",
										);
		foreach ($patterns as $pattern){
			$match = array();
			preg_match($pattern, $string, $match);
			
			if(!empty($match)){
				break;
			}
		}
		
		return trim($match[0]);
	}
	
	function propertyZipcode($propertyAddress){
		$text = "";
		$patterns = array("/([a-zA-Z]{2})\s([0-9\-]+)/");
		foreach ($patterns as $pattern){
			$match = array();
			preg_match($pattern, $propertyAddress, $match);
			
			if(!empty($match)){
				$removePattern = array("/[A-Z\s]+/");
				$text =  preg_replace($removePattern ,  "", $match[0]);
				break;
			}
		}
			return $text;
	}
	
	function propertyState($propertyAddress){
		$text = "";
		$patterns = array("/([a-zA-Z]{2})\s([0-9\-]+)/");
		foreach ($patterns as $pattern){
			$match = array();
			preg_match($pattern, $propertyAddress, $match);
			
			if(!empty($match)){
				$removePattern = array("/[0-9\s\-]+/");
				
				$text =  preg_replace($removePattern ,  "", $match[0]);
				break;
			}
		}
			return $text;
	}
	
	function tsSaleNumber($string){
		
		$text = "";
		$patterns = array("/TS\s#\s\:\s?([0-9\-]+)/");
		foreach ($patterns as $pattern){
			$match = array();
			preg_match($pattern, $string, $match);
			
			if(!empty($match)){
				$text = $match[1];
				break;
			}
		}
			return $text;
	}
	
	function propertyCountry($string){
			$text = "";
		$patterns = array("/([a-zA-Z\s]+)\sCounty/");
		foreach ($patterns as $pattern){
			$match = array();
			preg_match($pattern, $string, $match);
			
			if(!empty($match)){
				$removePattern = array("/\sCounty/");
				$text =  preg_replace($removePattern ,  "", $match[0]);
				break;
			}
		}
			return $text;
	}
	
	function salePlace($string){
				$text ="";
			$patterns = array( "/([0-9]{1,5})\s?([a-zA-Z0-9\,\.\#\&\;\s\-\)\(]+)([0-9]{5})/",
								"/([0-9]{1,5})\s?([a-zA-Z0-9\,\.\#\&\;\s\-\)\(]+)/",
								);
			foreach ($patterns as $pattern){
				$match = array();
				preg_match($pattern, $string, $match);
				
				if(!empty($match)){
					
					$text = trim($match[0]);
					break;
				}
			}
			
			return $text;
		
	}
	function saleDate($string){
			return date('Y-m-d H:i:s', strtotime($string));
		}	
	
} //end of class
?>