<?php
class Lpsasap {
	#var $_csvLocation = "";

	function __construct(){
		
	}
	
	function convertToArray($csvLocation){
		$data = array();
		$file = fopen($csvLocation, 'r');
		while (($line = fgetcsv($file)) !== FALSE) {
			$data[] = $line;

		}
		fclose($file);
		
		return $data;
	}
	
	function zipCode($string)
	{
		if(strlen($string) > 5):
			return preg_replace('/(\d{5})(\d{1,})/', '$1-$2', $string);	
		else:
			return $string;
		endif;	
	}
	
	function salePlace($string, $state){
			$text = "";
			$text = $new_str = preg_replace('/^[^\d]*/', '', $string);
			preg_match("/(\d{5})/",$text,$zipcode);
			if(isset($zipcode[0]) && !empty($zipcode[0])):
					$text = preg_replace('/'.$zipcode[0].'([^\s]*\s*\w*)*/','',$text);
					if($text == $zipcode[0]):
						return $string;
					else:
						return $text." ".$zipcode[0];
					endif;
			elseif( strpos($text, $state) !== false):
				$text = preg_replace('/'.$state.'([^\s]*\s*\w*)*/','',$text);
				return $text." ".$state;	
			else:
				return $string;	
			endif;
		}
		
		
	function saleDate($string){
			return date('Y-m-d H:i:s', strtotime($string));
		}	
	
} //end of class
?>