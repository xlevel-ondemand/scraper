<?php 
class Database
{
	private $hostname = HOST;
	private $database = DATABASE;
	private $username = USER;
	private $password = PASS;
	private $connectlink;	//Database Connection Link
	

	public function __construct() {
		$this->connectlink = mysql_connect($this->hostname,$this->username,$this->password);
		if(!($this->connectlink)) {
			throw new DatabaseConnectionException("Error Connecting to the Database". mysql_error(),"101");
		}
		else {
			mysql_select_db($this->database);
		}
	}
	 
	public function __destruct() {
		@mysql_close($this->connectlink);
	}
	
	function querySelect($qry)
    {
        $i=0;
        $data=array();
        $qry_result=mysql_query($qry) or die("QUERY ERROR SELECT=>".mysql_error());
        //$qry_row=mysql_fetch_assoc($qry_result);
        while ($row=mysql_fetch_assoc($qry_result)) {
            $data[$i] = $row;
            $i++;
        }
        return $data;
    }


    function querySelectSingle($qry)
    {
        $qry_result=mysql_query($qry) or die("QUERY ERROR SELECT SINGLE=>".mysql_error());
        $row=mysql_fetch_assoc($qry_result);
         return $row;
    }



    function numRows($nr)
    {
        $qry_result1=mysql_query($nr) or die("QUERY ERROR NUM ROWS=>".mysql_error());
        $num_row=mysql_num_rows($qry_result1);
        return $num_row;

    }
    function queryDelete($qd)
    {
        $result_delete=mysql_query($qd) or die("QUERY ERROR DELETE=>".mysql_error());
        return $result_delete;
    }
    function queryInsert($qd)
    {
        $result_insert=mysql_query($qd) or die("QUERY ERROR INSERT=>".mysql_error());
        return $result_insert;
    }
    function queryUpdate($qd)
    {
        $result_update=mysql_query($qd) or die("QUERY ERROR UPDATE=>".mysql_error());
        return $result_update;
    }
    function queryExecute($query)
    {
        $result_update=mysql_query($query) or die("QUERY ERROR EXECUTE=>".mysql_error());
        return mysql_affected_rows();
    }
 
}
 

?>