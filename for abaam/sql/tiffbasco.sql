-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 20, 2012 at 05:00 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_phrets`
--

-- --------------------------------------------------------

--
-- Table structure for table `update_tiffbasco`
--

CREATE TABLE IF NOT EXISTS `update_tiffbasco` (
  `original_sale_date` date NOT NULL,
  `projected_sale_date` date NOT NULL,
  `sale_time` time NOT NULL,
  `sale_place` varchar(500) NOT NULL,
  `property_address` varchar(100) NOT NULL,
  `county` varchar(50) NOT NULL,
  `loan_date` date NOT NULL,
  `mortgage_company` varchar(100) NOT NULL,
  `original_loan_amount` varchar(50) NOT NULL,
  `file_status` varchar(50) NOT NULL,
  `opening_bid` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
