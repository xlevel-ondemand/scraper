<?php
class TiffBasco {
	#var $_csvLocation = "";

	function __construct(){
		
	}
	
	function convertToArray($csvLocation){
		$data = array();
		$file = fopen($csvLocation, 'r');
		while (($line = fgetcsv($file)) !== FALSE) {
			$data[] = $line;

		}
		fclose($file);
		
		return $data;
	}
	
	function saleDate($string)
	{
		$org_sale = $proj_sale = 0;
		$sale_date = explode("(",$string);
		$org_sale = date('Y-m-d', strtotime($sale_date[0]));
		if(sizeof($sale_date)>1):
			$proj = str_replace(")","",$sale_date[1]);
			$proj_sale = date('Y-m-d', strtotime($proj));
			return $org_sale."::".$proj_sale;
		endif;	
		return $org_sale;
	}
	
	function normalDate($string)
	{
		return date('Y-m-d', strtotime($string));	
	}
	
	function normalTime($string)
	{
		return date('H:i:s', strtotime($string));	
	}
	
	function zipCode($string)
	{
		if(strlen($string) > 5):
			return preg_replace('/(\d{5})(\d{1,})/', '$1-$2', $string);	
		else:
			return $string;
		endif;	
	}
	
	function salePlace($string, $state){
			$text = "";
			$org_string = $string;
			$string = substr($string,30);
			$text = $new_str = preg_replace('/^[^\d]*/', '', $string);
			preg_match("/(\d{5})/",$text,$zipcode);
			if(isset($zipcode[0]) && !empty($zipcode[0])):
					$text = preg_replace('/'.$zipcode[0].'([^\s]*\s*\w*)*/','',$text);
					if($text == $zipcode[0]):
						return $string;
					else:
						return $text." ".$zipcode[0];
					endif;
			elseif( strpos($text, $state) !== false):
				$text = preg_replace('/'.$state.'([^\s]*\s*\w*)*/','',$text);
				return $text." ".$state;	
			else:
				return $text;	
			endif;
		}
		
		
	
	
} //end of class
?>