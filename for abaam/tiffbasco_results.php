
<?php
include 'class/Config.php';
include 'class/TiffBascoResults.php';
include 'class/DbConnect.php';

class TiffBascoResultsParser extends TiffBascoResults
{
	private $data;
	private $dir = "C:/ScrapeData/tiffbasco_results/";
	private $objqry;
	private $qryString;
	private $files;
	private $stateDir;
		
	public function __construct()
	{
		$this->objqry = new Database();
	}
	
	public function parseResults()
	{
		$this->emptyTable();
		
		$this->files = glob($this->dir."/*.csv");
		foreach($this->files as $file):
			if(isset($file) && !empty($file)):
				$this->data = parent::convertToArray($file); 
				$i = 0;
				$j = 0;
				foreach($this->data as $data):
					if($i>4):
						if($j < 3):
							switch($j){
								case 0:
									$sd = self::saleDate($data[0]); 
									$sd = explode("::", $sd);
									$this->qryString = "`original_sale_date` = '".$sd[0]."'";
									if(sizeof($sd)>1){
										$this->qryString .= ",`projected_sale_date` = '".$sd[1]."' ";
									}
									
									if(isset($data[2]) && !empty($data[2])):
										$this->qryString .=",`loan_date` = '".self::normalDate($data[2])."'";
									endif;	
									
									$this->qryString .= ",`mortgage_company` = '".mysql_real_escape_string($data[3])."'
												   ,`original_loan_amount` = '".mysql_real_escape_string($data[4])."'
												   ,`file_status` = '".mysql_real_escape_string($data[5])."'
												   ,`results` = '".mysql_real_escape_string($data[6])."' ";
									
									break;
								case 1:	
									$this->qryString .=",`property_address` = '".mysql_real_escape_string($data[0])."'";
									$this->qryString .=",`county` = '".mysql_real_escape_string($data[1])."'";
									$this->qryString .=",`default_date` = '".mysql_real_escape_string(self::normalDate($data[2]))."'"; 
									if(isset($data[4]) && !empty($data[4])):
										$this->qryString .=",`current_principal` = '".mysql_real_escape_string($data[4])."'";
									endif;	
									
									//echo $this->qryString; 
									$this->objqry->queryInsert("INSERT INTO `update_tiffbasco_results` SET ".$this->qryString);
									$this->qryString = "";
									echo "<br><br>";
									break;	
								
							}
							$j++;
						else: 
							$j = 0; 
						endif;
							
					endif;
						
					$i++;
					
				endforeach;	
				
			endif;
			
		endforeach;
	}
	
	
	
	private function emptyTable()
	{
		$this->objqry->queryExecute("TRUNCATE `update_lpsasap`");
	}
}

$tfparser = new TiffBascoResultsParser();
$tfparser->parseResults();

