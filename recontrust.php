<?php
include 'class/Config.php';
include 'class/Recontrust.php';
include 'class/DbConnect.php';

class RecontrustParser extends Recontrust
{
	private $data;
	private $dir = "C:/ScrapeData/recontrust/";
	private $objqry;
	private $qryString;
	private $files;
	private $stateDir;
		
	public function __construct()
	{
		$this->objqry = new Database();
	}
	
	
	public function parseRecontrust()
	{
		$this->stateDir = scandir($this->dir);
		
		$this->emptyTable();
		
		foreach($this->stateDir as $state):
			if(isset($state) && !empty($state)):
				$this->parseData($state);
			endif;
		endforeach;
	}
	
	private function parseData($state)
	{
		$this->files = glob($this->dir.$state."/*.csv");
		echo "<pre>+".$state."</pre>";
		foreach($this->files as $file):
			if(isset($file) && !empty($file)):
				$this->parse($file);
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+".$file."<br>";
			endif;
		endforeach;
	}
	
	private function parse($csvfile)
	{
		$this->data = parent::convertToArray($csvfile); 
		$i = 0;
		foreach($this->data as $data):
			if($i>0):
				$property_address = mysql_real_escape_string(self::propertyAddress($data[0]));	  
				$trustee_sale_number = mysql_real_escape_string(self::tsSaleNumber($data[0]));
				$county = mysql_real_escape_string(self::propertyCountry($data[0]));
				$opening_bid = mysql_real_escape_string($data[1]);
				$sale_date = mysql_real_escape_string(self::saleDate($data[3]));
				$sale_place = mysql_real_escape_string(self::salePlace($data[2], self::propertyState($data[0])));
				$system_date = date('Y-m-d');	  
				
				$hash_value = md5($property_address.$trustee_sale_number.$county.$opening_bid.$sale_date.$sale_place);
				
					  
				$this->qryString = "`property_address` = '".$property_address."', 
									`trustee_sale_number` = '".$trustee_sale_number."', 
									`county` = '".$county."', 
							   		`opening_bid` = '".$opening_bid."', 
									`sale_date` = '".$sale_date."', 
									`sale_place` = '".$sale_place."',
									`system_date` = '".$system_date."',
									`hash_value` = '".$hash_value."' ";
									
				$this->objqry->queryInsert("INSERT INTO `update_recontrust` SET ".$this->qryString);
			endif;
			$i++;
		endforeach;
	}
	
	private function emptyTable()
	{
		$this->objqry->queryExecute("TRUNCATE `update_recontrust`");
	}
}

$rparser = new RecontrustParser();
$rparser->parseRecontrust();

