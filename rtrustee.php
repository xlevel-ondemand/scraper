<?php
include 'class/Config.php';
include 'class/CsvParser.php';
include 'class/DbConnect.php';

class rtrustee extends CSVPARSER
{
	private $data;
	private $dir = "C:/ScrapeData/rtrustee/";
	private $objqry;
	private $qryString;
	private $files;
	private $stateDir;
		
	public function __construct()
	{
		$this->objqry = new Database();
	}
	
	
	public function parsertrustee()
	{
		$this->stateDir = scandir($this->dir);
		
		$this->emptyTable();
		
		foreach($this->stateDir as $state):
			if(isset($state) && !empty($state)):
				$this->parseData($state);
			endif;
		endforeach;
	}
	
	private function parseData($state)
	{
		$this->files = glob($this->dir.$state."/*.csv");
		echo "<pre>+".$state."</pre>";
		foreach($this->files as $file):
			if(isset($file) && !empty($file)):
				$this->parse($file);
				echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+".$file."<br>";
			endif;
		endforeach;
	}
	
	private function parse($csvfile)
	{
		$this->data = parent::convertToArray($csvfile); 
		$i = 0;
		foreach($this->data as $data):
			if($i>0):
				
				$this->qryString = "`property_address` = '".mysql_real_escape_string($data[23]." ".$data[24].", ".$data[25]." ".$data[26])."', 
									`trustee_sale_number` = '".mysql_real_escape_string($data[2])."', 
									`county` = '".mysql_real_escape_string($data[22])."', 
							   		
									`sale_date` = '".mysql_real_escape_string($data[5])."', 
									`sale_place` = '".mysql_real_escape_string(self::salePlace($data[8]))."',
									`status` = '".mysql_real_escape_string($data[15])."',
									`system_date` = '2012-12-25'";
								
									
				$this->objqry->queryInsert("INSERT INTO `update_rtrustee` SET ".$this->qryString);
				
				
				
				echo '<br>';
				
			endif;
			$i++;
		endforeach;
	}
	

	
	private function emptyTable()
	{
		$this->objqry->queryExecute("TRUNCATE `update_rtrustee`");
	}
}

$parser = new rtrustee();
$parser->parsertrustee();

